/*
 * Signal Processing Devices ADQ PCIe/PXIe/uTCA devices
 *
 * Signal Processing Devices Sweden AB <support@spdevices.com>
 *
 * Device driver for PCI-express, PXI-express and
 * micro-TCA variants of ADQ products.
 *
 * This file is licensed under the terms of the GNU General Public License
 * version 2.  This program is licensed "as is" without any warranty of any
 * kind, whether express or implied.
 */

/* #define DEBUG */
/* #define ADQ_TRACE */
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/init.h>
#include <linux/version.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/cdev.h>
#include <linux/semaphore.h>
#include <linux/interrupt.h>
#include <linux/vmalloc.h>
#include <linux/wait.h>
#include <linux/workqueue.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
#include <asm/uaccess.h>
#else
#include <linux/uaccess.h>
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,35)
#define pr_warn pr_warning
#endif

#include "spd_adq_pci_ioctl.h"

#define PCI_VENDOR_ID_SPD          (0x1B37)

#define PCI_DEVICE_ID_SPD_ADQDSP   (0x000F)
#define PCI_DEVICE_ID_SPD_ADQ214   (0x0001)
#define PCI_DEVICE_ID_SPD_ADQ212   (0x0015)
#define PCI_DEVICE_ID_SPD_ADQ114   (0x0003)
#define PCI_DEVICE_ID_SPD_ADQ112   (0x0005)
#define PCI_DEVICE_ID_SPD_ADQ108   (0x000e)
#define PCI_DEVICE_ID_SPD_ADQ412   (0x0014)
#define PCI_DEVICE_ID_SPD_SDR14    (0x001B)
#define PCI_DEVICE_ID_SPD_ADQ1600  (0x001C)
#define PCI_DEVICE_ID_SPD_SphinxAA (0x0011)
#define PCI_DEVICE_ID_SPD_DSU      (0x001F)
#define PCI_DEVICE_ID_SPD_ADQ208   (0x001E)
#define PCI_DEVICE_ID_SPD_ADQ14    (0x0020)
#define PCI_DEVICE_ID_SPD_ADQ12    (0x0027)
#define PCI_DEVICE_ID_SPD_ADQ7     (0x0023)
#define PCI_DEVICE_ID_SPD_ADQ8     (0x0026)

/* Supported devices */
static struct pci_device_id ids[] = {
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQDSP),
   .driver_data = (kernel_ulong_t) "ADQ DSP"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ214),
   .driver_data = (kernel_ulong_t) "ADQ214"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ212),
   .driver_data = (kernel_ulong_t) "ADQ212"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ114),
   .driver_data = (kernel_ulong_t) "ADQ114"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ112),
   .driver_data = (kernel_ulong_t) "ADQ112"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ108),
   .driver_data = (kernel_ulong_t) "ADQ108"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ412),
   .driver_data = (kernel_ulong_t) "ADQ412"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_SDR14),
   .driver_data = (kernel_ulong_t) "SDR14"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ1600),
   .driver_data = (kernel_ulong_t) "ADQ1600"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_SphinxAA),
   .driver_data = (kernel_ulong_t) "Sphinx AA"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_DSU),
   .driver_data = (kernel_ulong_t) "DSU"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ208),
   .driver_data = (kernel_ulong_t) "ADQ208"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ14),
   .driver_data = (kernel_ulong_t) "ADQ14"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ12),
   .driver_data = (kernel_ulong_t) "ADQ12"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ7),
   .driver_data = (kernel_ulong_t) "ADQ7"},
  {PCI_DEVICE(PCI_VENDOR_ID_SPD, PCI_DEVICE_ID_SPD_ADQ8),
   .driver_data = (kernel_ulong_t) "ADQ8"},
  {0,}
};

MODULE_DEVICE_TABLE(pci, ids);

/* Maximum number of devices */
#define ADQ_DEV_MAX 1000
/* Filename for /dev and corresponding maximum length name */
#define ADQ_DEV_NAME "adq_pcie"
#define MAX_ADQ_DEV_NAME "adq_pcie_999"
/* Maximum number of allocated buffers */
#define ADQ_DMA_BUF_MAX 256
#define ADQ_DMA_BIT_MASK DMA_BIT_MASK(32)

#define ADQ_FIRST_CHRDEV_MINOR 0
#define ADQ_NUMBER_CHRDEV 1

#define DMA_HW_QUEUE_SIZE 32

#define ADQ_WRITE_BUFSIZE 128
#define ADQ_READ_BUFSIZE  128
#define ADQ_BAR2_USE_WRITE_COMBINING 0

struct adq_dma {
  /* Physical addresses for DMA buffers */
  dma_addr_t handle[ADQ_DMA_BUF_MAX];
  /* Virtual addresses for DMA buffers */
  void *buf[ADQ_DMA_BUF_MAX];
  /* Expected TLP count for buffer write from card */
  u32 tlp_count[ADQ_DMA_BUF_MAX];
  /* Allocated size of each DMA buffer */
  u32 buf_size;
  /* Number of allocated DMA buffers */
  u32 nalloc;
  /* Current write buffer */
  u32 wptr;
  /* Current read buffer */
  u32 rptr;
  /* Number of available write buffers */
  struct semaphore sem_w;
  /* Number of available read buffers */
  struct semaphore sem_r;
  /* Number of buffers with data */
  atomic_t count_r;
  /* Number of buffers waiting for data */
  atomic_t count_w;
  /* Scheduled device DMA requests */
  u32 sched_tlp_count;
  u32 sched_count;
  /* Mutex for DMA meta data */
  struct semaphore sem;
  /* Flag for aborting DMA workers */
  atomic_t abort_dma;
};

struct adq_dev {
  /* Enumeration used for device file */
  unsigned int id;
  /* BAR0 address and length */
  unsigned long bar0_addr;
  unsigned long bar0_len;
  /* BAR2 address and length */
  unsigned long bar2_addr;
  unsigned long bar2_len;
  /* Allow targeting of both BAR0 and BAR2 in adq_write */
  unsigned long bar_target_addr;
  unsigned long bar_target_len;


  /* Maximum packet size read out from card */
  unsigned int mpss;
  /* Number of available DMA channels */
  u8 ndma;

  /* IRQ is allocated */
  u8 irq_alloc;

  /* PCIe device struct */
  struct pci_dev *pci_dev;

  /* DMA scheduler worker */
  struct work_struct schedule_dma_work;

  /* Dedicated workqueue */
  struct workqueue_struct *workqueue;

  /* Keep track of any bottom handlers started */
  wait_queue_head_t wq_bottoms;

  atomic_t bottoms_count;

  /* Char file structs */
  dev_t fs_dev;
  struct cdev c_dev;
  struct class *cl;
  struct device *cd;

  /* Name of char file */
  char name[sizeof(MAX_ADQ_DEV_NAME)];

  /* DMA buffer related meta data */
  struct adq_dma dma;
};

/* Functions */
static void __exit pci_adq_exit(void);
static int __init pci_adq_init(void);
static void remove(struct pci_dev *dev);
static int probe(struct pci_dev *dev, const struct pci_device_id *id);
static ssize_t adq_write(struct file *filp, const char __user *buff,
                         size_t count, loff_t *offp);
static int adq_mmap(struct file *filp, struct vm_area_struct *vma);
static int adq_release(struct inode *i, struct file *filp);
static int adq_open(struct inode *i, struct file *filp);
static long adq_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
static void issue_scheduled_dma(struct adq_dev *adq);
static void request_device_dma(struct adq_dev *adq,
                               struct adq_ioctl_dma *dmareq);
static int allocate_dma_buf(struct adq_dev *adq,
                            struct adq_ioctl_alloc *alloc_struct);
static void unallocate_dma_buf(struct adq_dev *adq);
static irqreturn_t adq_interrupt(int irq, void *dev);
static void schedule_dma(struct work_struct *taskp);

static __always_inline int reserve_dma_slot(struct adq_dev *adq,
                                            struct semaphore *sem);

/* Number of probed ADQ devices */
static atomic_t adq_num;

/* Debug function, dumps all register values to kernel log */
#ifdef DEBUG
static void pr_device_registers(struct adq_dev *adq)
{
  u8 ch;
  u32 reg;

  reg = ioread32((u32 __iomem *) (adq->bar0_addr + XILREG_DCSR_OFFSET));
  pr_devel("XILREG_DCSR_OFFSET:   %08x\n", reg);
  pr_devel("  RST=%d\n", (reg & 1));
  pr_devel("  Version=%d\n", (reg >> 8) & 0xff);
  reg = ioread32((u32 __iomem *) (adq->bar0_addr + XILREG_DLWSR_OFFSET));
  pr_devel("XILREG_DLWSR_OFFSET:  %08x\n", reg);
  pr_devel("  Link cap=%d\n", (reg & 0xff));
  pr_devel("  Link neg=%d\n", (reg >> 8 & 0xff));
  reg = ioread32((u32 __iomem *) (adq->bar0_addr + XILREG_DLTSR_OFFSET));
  pr_devel("XILREG_DLTSR_OFFSET:  %08x\n", reg);
  pr_devel("  Cap Max Payload Size=%d\n", (reg & 0xf));
  pr_devel("  Read Comp Bound=%d\n", (reg >> 4 & 0xf));
  pr_devel("  Prog Max Payload Size=%d\n", (reg >> 8 & 0xff));
  pr_devel("  Max Read Request Size=%d\n", (reg >> 16 & 0xff));
  reg = ioread32((u32 __iomem *) (adq->bar0_addr + XILREG_DLTCR_OFFSET));
  pr_devel("XILREG_DLTCR_OFFSET:  %08x\n", reg);
  pr_devel("  CPL Streaming=%d\n", (reg & 1));
  pr_devel("  Read metering=%d\n", (reg >> 1 & 1));
  pr_devel("  Transaction RNP OK=%d\n", (reg >> 8 & 0xff));
  pr_devel("  Memory Write WRR count=%d\n", (reg >> 16 & 0xff));
  pr_devel("  Memory Read WRR count=%d\n", (reg >> 24 & 0xff));

  for (ch = 0; ch < adq->ndma; ch++) {
    reg = ioread32((u32 __iomem *) (adq->bar0_addr +
                                    XILREG_CH(ch, XILREG_WDPCR_OFFSET)));
    pr_devel("XILREG_WDPCR_OFFSET(%d):  %08x\n", ch, reg);
    pr_devel("  Memory Write Relaxed Order=%d\n", (reg & 1));
    pr_devel("  Memory Write No Snoop=%d\n", (reg >> 1 & 1));
    pr_devel("  Memory Write TLP TC=%d\n", (reg >> 2 & 0x7));
    pr_devel("  Memory Write PHANT FUNC EN=%d\n", (reg >> 6 & 1));
    pr_devel("  Memory Write Constant Address=%d\n", (reg >> 7 & 1));
    pr_devel("  Memory Write Length=%d\n", (reg >> 16) & 0xffff);
    reg = ioread32((u32 __iomem *) (adq->bar0_addr +
                                    XILREG_CH(ch, XILREG_WDICSR_OFFSET)));
    pr_devel("XILREG_WDICSR_OFFSET(%d): %08x\n", ch, reg);
    pr_devel("  Memory Write Interrupt Disable=%d\n", (reg & 1));
    pr_devel("  Memory Write Interrupt=%d\n", (reg >> 1 & 1));
    pr_devel("  Memory Write Done Count=%d\n", (reg >> 8 & 0xff));
    reg = ioread32((u32 __iomem *) (adq->bar0_addr +
                                    XILREG_CH(ch,XILREG_WDLACR_OFFSET)));
    pr_devel("XILREG_WDLACR_OFFSET(%d): %08x\n", ch, reg);
    pr_devel("  DMA Write Address Low =0x%08x\n", reg);
    reg = ioread32((u32 __iomem *) (adq->bar0_addr +
                                    XILREG_CH(ch, XILREG_WDUACR_OFFSET)));
    pr_devel("XILREG_WDUACR_OFFSET(%d): %08x\n", ch, reg);
    pr_devel("  DMA Write Address High =0x%08x\n", reg);
    reg = ioread32((u32 __iomem *) (adq->bar0_addr +
                                    XILREG_CH(ch, XILREG_WDCCR_OFFSET)));
    pr_devel("XILREG_WDCCR_OFFSET(%d):  %08x\n", ch, reg);
    pr_devel("  DMA Write Packet Count=%d\n", reg);
    reg = ioread32((u32 __iomem *) (adq->bar0_addr +
                                    XILREG_CH(ch, XILREG_WDSCSR_OFFSET)));
    pr_devel("XILREG_WDSCSR_OFFSET(%d): %08x\n", ch, reg);
    pr_devel("  Memory Write Start=%d\n", (reg & 1));
    pr_devel("  Memory Write Queue Count=%d\n", (reg >> 8 & 0xff));
  }
}
#endif

static void schedule_dma(struct work_struct *taskp)
{
  struct adq_dev *adq = container_of(taskp, struct adq_dev,
    schedule_dma_work);

  issue_scheduled_dma(adq);
}

/* Device interrupt handler */
static irqreturn_t adq_interrupt(int irq, void *dev)
{
  struct adq_dev *adq = dev;
  unsigned int ch;
  unsigned int handled = 0;

  /* Check for DMA interrupt in device */
  for (ch = 0; ch < adq->ndma; ch++) {
    if (ioread32 ((u32 __iomem *) (adq->bar0_addr +
                                   XILREG_CH(ch, XILREG_WDICSR_OFFSET)))
        & XILREG_WDICSR_INT) {
      /*
       * Write to the WDICSR register to acknowledge the interrupt
       * (clear "Write DMA Interrupt" by writing 1 to interrupt
       * bit XILREG_WDICSR_INT)
       */
      iowrite32(XILREG_WDICSR_INT,
                (u32 __iomem *) (adq->bar0_addr +
                                 XILREG_CH(ch, XILREG_WDICSR_OFFSET)));

      if (ch == 0) {
#ifdef ADQ_TRACE
        trace_printk("%s adq_interrupt\n", pci_name(adq->pci_dev));
#endif
        /* Increase read semaphore, a new buffer can be read */
        up(&adq->dma.sem_r);
        atomic_inc(&(adq->dma.count_r));
        atomic_dec(&(adq->dma.count_w));

        if (!atomic_read(&(adq->dma.abort_dma))) {
          atomic_inc(&(adq->bottoms_count));

          /* Issue as many scheduled transfers as possible,
           * due to free device interrupt slots  */
          queue_work(adq->workqueue, &adq->schedule_dma_work);

          atomic_dec(&(adq->bottoms_count));
          wake_up(&(adq->wq_bottoms));
        }
      }
      handled = 1;
    }
  }

  /* Return IRQ_HANDLED if we handled the interrupt */
  if (handled)
    return IRQ_HANDLED;
  else
    return IRQ_NONE;
}

/* Unallocate DMA buffers and free interrupt */
static void unallocate_dma_buf(struct adq_dev *adq)
{
  int i;

  if (adq->dma.nalloc == 0) {
    return;
  }

  /* Free IRQ */
  dev_dbg(&adq->pci_dev->dev, "Freeing IRQ %d for device.\n",
          adq->pci_dev->irq);
  if (adq->irq_alloc == 1) {
    free_irq(adq->pci_dev->irq, adq);
    adq->irq_alloc = 0;
  }

  for (i = 0; i < adq->dma.nalloc; i++) {
    pci_free_consistent(adq->pci_dev, adq->dma.buf_size,
                        adq->dma.buf[i], adq->dma.handle[i]);
    dev_dbg(&adq->pci_dev->dev, "Freeing buffer %d\n", i);
  }
  adq->dma.nalloc = 0;
  adq->dma.rptr = 0;
  adq->dma.wptr = 0;
  adq->dma.sched_count = 0;
  adq->dma.sched_tlp_count = 0;
  sema_init(&adq->dma.sem_w, 0);
  sema_init(&adq->dma.sem_r, 0);
  atomic_set(&(adq->dma.count_r), 0);
  atomic_set(&(adq->dma.count_w), 0);
}

/* Allocate DMA buffers and register interrupt handler */
static int allocate_dma_buf(struct adq_dev *adq,
                            struct adq_ioctl_alloc *alloc_struct)
{
  int i;
  int n_allocated;

  alloc_struct->size = ((alloc_struct->size + PAGE_SIZE - 1)
                        / PAGE_SIZE * PAGE_SIZE);

  dev_dbg(&adq->pci_dev->dev, "Allocating %d buffers of size %d\n",
          alloc_struct->number, alloc_struct->size);

  down(&adq->dma.sem);
  adq->dma.buf_size = alloc_struct->size;
  for (i = 0; i < alloc_struct->number; i++) {
    if (i == ADQ_DMA_BUF_MAX)
      break;
    adq->dma.buf[i] = pci_alloc_consistent(adq->pci_dev, adq->dma.buf_size,
                                           &adq->dma.handle[i]);
    if (adq->dma.buf[i] == NULL) {
      unallocate_dma_buf(adq);
      up(&adq->dma.sem);
      return -ENOMEM;
    }
    adq->dma.nalloc++;
    dev_dbg(&adq->pci_dev->dev, "Allocated %d\n", i + 1);
  }

  /* Initialize buffer pointers */
  adq->dma.rptr = adq->dma.nalloc - 1;
  adq->dma.wptr = adq->dma.nalloc - 1;
  sema_init(&adq->dma.sem_w, adq->dma.nalloc - 1);
  sema_init(&adq->dma.sem_r, 0);
  atomic_set(&(adq->dma.count_r), 0);
  atomic_set(&(adq->dma.count_w), 0);
  adq->dma.sched_count = 0;
  adq->dma.sched_tlp_count = 0;
  n_allocated = adq->dma.nalloc;
  up(&adq->dma.sem);

  if (adq->irq_alloc == 0) {
    /* Register IRQ handler */
    dev_dbg(&adq->pci_dev->dev, "Requesting IRQ %d for device.\n",
            adq->pci_dev->irq);
    if (request_irq (adq->pci_dev->irq, adq_interrupt, IRQF_SHARED,
                     adq->name, adq)) {
      dev_err(&adq->pci_dev->dev, "Failed to request IRQ %d for device at bus"
              " %d slot %d).\n", adq->pci_dev->irq, adq->pci_dev->bus->number,
              PCI_SLOT(adq->pci_dev->devfn));
      return -EBUSY;
    }
    adq->irq_alloc = 1;
  }

  return 0;
}

static void request_device_dma(struct adq_dev *adq,
                               struct adq_ioctl_dma *dmareq)
{
  u32 addr_hi;
  u32 addr_lo;

  /* Update write pointer */
  adq->dma.wptr = (adq->dma.wptr + 1) % adq->dma.nalloc;
  atomic_inc(&(adq->dma.count_w));
  /* Store requested size for later readout */
  adq->dma.tlp_count[adq->dma.wptr] = dmareq->tlp_count;
#ifdef ADQ_TRACE
  trace_printk("%s DMA Write request sched: %d\n", pci_name(adq->pci_dev),
               adq->dma.sched_count);
#endif

  addr_hi = ((adq->dma.handle[adq->dma.wptr] >> 32) & 0xffffffff);
  addr_lo = (adq->dma.handle[adq->dma.wptr] & 0xffffffff);

  /* Request transfer from device */
  /* Set upper 32bit of DMA address */
  iowrite32(addr_hi,
            (u32 __iomem *) (adq->bar0_addr +
                             XILREG_CH(0, XILREG_WDUACR_OFFSET)));
  /* Set lower 32bit of DMA address */
  iowrite32(addr_lo,
            (u32 __iomem *) (adq->bar0_addr +
                             XILREG_CH(0, XILREG_WDLACR_OFFSET)));
  /* Set TLP count */
  iowrite32(dmareq->tlp_count,
            (u32 __iomem *) (adq->bar0_addr +
                             XILREG_CH(0, XILREG_WDCCR_OFFSET)));

  /* Start DMA Transfer */
  iowrite32(XILREG_WDSCSR_DMA,
            (u32 __iomem *) (adq->bar0_addr +
                             XILREG_CH(0, XILREG_WDSCSR_OFFSET)));
}

static void issue_scheduled_dma(struct adq_dev *adq)
{
  struct adq_ioctl_dma dmareq;

  down(&adq->dma.sem);
  if (adq->dma.sched_count == 0) {
    up(&adq->dma.sem);
    return;
  }
  dmareq.tlp_count = adq->dma.sched_tlp_count;
  up(&adq->dma.sem);

  /* Issue as many scheduled transfers as possible */
  while (reserve_dma_slot(adq, &adq->dma.sem) == 0) {
    if (down_trylock(&adq->dma.sem_w)) {
      up(&adq->dma.sem);
      break;
    }

    request_device_dma(adq, &dmareq);
    adq->dma.sched_count--;
    if (adq->dma.sched_count == 0) {
      up(&adq->dma.sem);
      break;
    }
    up(&adq->dma.sem);
  }
}

/* Check if HW DMA queue can accept more transfers.
   and decrease semaphore if so.
   Returns the queue value. */
static __always_inline int reserve_dma_slot(struct adq_dev *adq,
                                            struct semaphore *sem)
{
  u32 size;

  down(sem);
  size = (ioread32((u32 __iomem *) (adq->bar0_addr +
                   XILREG_CH(0, XILREG_WDSCSR_OFFSET))) >> 8) & 0x3f;
  if (size >= DMA_HW_QUEUE_SIZE)
  {
    up(sem);
    return -1;
  }
  return 0;
}

/* IOCTLs for character device */
static long adq_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
  union {
    struct adq_ioctl_reg device_reg;
    struct adq_ioctl_device_info device_info;
    struct adq_ioctl_response response;
    struct adq_ioctl_alloc alloc_struct;
    struct adq_ioctl_buf buf_struct;
    struct adq_ioctl_dma dma_struct;
    struct adq_ioctl_csh csh;
    struct adq_ioctl_abort abort;
  } s;
  struct adq_dev *adq;
  int result, reg;
  int i, ch;

  /* Fetch ADQ device struct pointer */
  adq = filp->private_data;

  switch (cmd) {

  case ADQ_IOCTL_DEVICE_RD_REG:
    if (copy_from_user(&s, (struct adq_ioctl_reg __user *)arg,
                       sizeof(struct adq_ioctl_reg)))
      return -EFAULT;

    if (s.device_reg.offset >= adq->bar0_len) {
      dev_warn(&adq->pci_dev->dev, "ADQ_IOCTL_DEVICE_RD_REG: Tried to access "
               "0x%x which is outside of limit (0x%lx)\n",
               s.device_reg.offset, adq->bar0_len);
      return -EINVAL;
    }
    s.device_reg.val =
      ioread32((u32 __iomem *) (adq->bar0_addr +
                                s.device_reg.offset));

    if (copy_to_user((struct adq_ioctl_reg __user *)arg, &s,
                     sizeof(struct adq_ioctl_reg)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_DEVICE_WR_REG:
    if (copy_from_user(&s, (struct adq_ioctl_reg __user *)arg,
                       sizeof(struct adq_ioctl_reg)))
      return -EFAULT;
    if (s.device_reg.offset >= adq->bar0_len) {
      dev_warn(&adq->pci_dev->dev, "ADQ_IOCTL_DEVICE_RD_REG: Tried to write to "
               "0x%x which is outside of limit (0x%lx)\n",
               s.device_reg.offset, adq->bar0_len);
      return -EINVAL;
    }
    s.device_reg.val = ((s.device_reg.mask & s.device_reg.val) |
                        ((~s.device_reg.mask) &
                         ioread32((u32 __iomem *)
                                  (adq->bar0_addr + s.device_reg.offset))));
    iowrite32(s.device_reg.val,
              (u32 __iomem *) (adq->bar0_addr +
                               s.device_reg.offset));
    break;

  case ADQ_IOCTL_DRIVER_REV:
    s.response.val = ADQ_DEVICE_DRIVER_REVISION_MAJOR << 16;
    s.response.val |= ADQ_DEVICE_DRIVER_REVISION_MINOR;
    if (copy_to_user((struct adq_ioctl_response __user *)arg, &s,
                     sizeof(struct adq_ioctl_response)))
      return -EFAULT;

    dev_dbg(&adq->pci_dev->dev, "Revision request\n");
    break;

  case ADQ_IOCTL_DEVICE_PHYSADDR:
    if (copy_from_user(&s, (struct adq_ioctl_response __user *)arg,
                       sizeof(struct adq_ioctl_response)))
      return -EFAULT;

    if (s.response.val > 6)
      return -EINVAL;

    s.response.val =
      (__u32) pci_resource_start(adq->pci_dev, s.response.val);

    if (copy_to_user((struct adq_ioctl_response __user *)arg, &s,
                     sizeof(struct adq_ioctl_response)))
      return -EFAULT;
    dev_dbg(&adq->pci_dev->dev, "Physaddr request\n");
    break;

  case ADQ_IOCTL_DEVICE_INFO:
#ifdef DEBUG
    pr_device_registers(adq);
#endif
    pr_devel("ADQ device info request\n");
    pr_devel("  VID = 0x%04x\n", adq->pci_dev->vendor);
    s.device_info.vid = adq->pci_dev->vendor;
    pr_devel("  PID = 0x%04x\n", adq->pci_dev->device);
    s.device_info.pid = adq->pci_dev->device;
    pr_devel("  MPSS = %d\n", adq->mpss);
    s.device_info.mpss = adq->mpss;
    pr_devel("  Bus = %d\n", adq->pci_dev->bus->number);
    s.device_info.bus = adq->pci_dev->bus->number;
    pr_devel("  Slot = %d\n", PCI_SLOT(adq->pci_dev->devfn));
    s.device_info.slot = PCI_SLOT(adq->pci_dev->devfn);

    if (copy_to_user((struct adq_ioctl_device_info __user *)arg, &s,
                     sizeof(struct adq_ioctl_device_info)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_ALLOC_DMA_BUF:
    if (copy_from_user(&s, (struct adq_ioctl_alloc __user *)arg,
                       sizeof(struct adq_ioctl_alloc)))
      return -EFAULT;

    /* Deallocate any buffers previously allocated */
    if (down_interruptible(&adq->dma.sem))
      return -EINTR;
    unallocate_dma_buf(adq);
    up(&adq->dma.sem);

    /* Allocate new buffers */
    if ((result =
         allocate_dma_buf(adq, (struct adq_ioctl_alloc *)&s)))
      return result;

    if (copy_to_user((struct adq_ioctl_alloc __user *)arg, &s,
                     sizeof(struct adq_ioctl_alloc)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_FETCH_DMA_BUF:
    if (copy_from_user(&s, (struct adq_ioctl_buf __user *)arg,
                       sizeof(struct adq_ioctl_buf)))
      return -EFAULT;
    s.buf_struct.size = 0;
    s.buf_struct.mmap_offset = 0;
    s.buf_struct.bus_addr_hi = 0;
    s.buf_struct.bus_addr_lo = 0;
    if (down_interruptible(&adq->dma.sem))
      return -EINTR;
    if (s.buf_struct.buf_num < adq->dma.nalloc) {
      s.buf_struct.size = adq->dma.buf_size;
      s.buf_struct.mmap_offset =
        s.buf_struct.buf_num * PAGE_SIZE;
      s.buf_struct.bus_addr_hi =
        (__u32) ((adq->dma.handle[s.buf_struct.buf_num] >> 32) & 0xFFFFFFFF);
      s.buf_struct.bus_addr_lo =
        (__u32) ((adq->dma.handle[s.buf_struct.buf_num]) & 0xFFFFFFFF);
    }
    up(&adq->dma.sem);
    if (copy_to_user((struct adq_ioctl_response __user *)arg, &s,
                     sizeof(struct adq_ioctl_buf)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_REQUEST_DMA_WRITE:
    if (copy_from_user(&s, (struct adq_ioctl_dma __user *)arg,
                       sizeof(struct adq_ioctl_dma)))
      return -EFAULT;
    /* Check that buffers are large enough */
    if (down_interruptible(&adq->dma.sem))
      return -EINTR;
    if (s.dma_struct.tlp_count * adq->mpss > adq->dma.buf_size) {
      up(&adq->dma.sem);
      return -ENOMEM;
    }
    /* Check that count is more than zero */
    if (s.dma_struct.count == 0) {
      up(&adq->dma.sem);
      return -EINVAL;
    }
    /* Check if schedule queue is empty, if not just schedule more transfers */
    if (adq->dma.sched_count > 0) {
      if (adq->dma.sched_tlp_count != s.dma_struct.tlp_count) {
        up(&adq->dma.sem);
        dev_err(&adq->pci_dev->dev,
                "Requested TLP count doesn't match enqueued value. queued: "
                "%d != %d\n", adq->dma.sched_tlp_count, s.dma_struct.tlp_count);
        return -EINVAL;
      }
      adq->dma.sched_count += s.dma_struct.count;
      up(&adq->dma.sem);
      break;
    }
    up(&adq->dma.sem);

    /* Issue as many transfers as possible */
    while (s.dma_struct.count > 0 &&
           reserve_dma_slot(adq, &adq->dma.sem) == 0) {
      if (down_trylock(&adq->dma.sem_w)) {
        up(&adq->dma.sem);
        break;
      }
      request_device_dma(adq, &s.dma_struct);
      up(&adq->dma.sem);
      s.dma_struct.count--;
    }

    if (down_interruptible(&adq->dma.sem))
      return -EINTR;
    /* Schedule more requests when buffers free up */
    if (s.dma_struct.count > 0) {
      if ((adq->dma.sched_count > 0) &&
          (adq->dma.sched_tlp_count !=
           s.dma_struct.tlp_count)) {
        dev_err(&adq->pci_dev->dev,
                "Requested TLP count doesn't match enqueued value at schedule. "
                "%d != %d\n", adq->dma.sched_tlp_count, s.dma_struct.tlp_count);
        up(&adq->dma.sem);
        return -EINVAL;
      }
      adq->dma.sched_count += s.dma_struct.count;
      adq->dma.sched_tlp_count = s.dma_struct.tlp_count;
    }
    up(&adq->dma.sem);

    queue_work(adq->workqueue, &adq->schedule_dma_work);

    break;

  case ADQ_IOCTL_REQUEST_DMA_P2P:
    if (copy_from_user(&s, (struct adq_ioctl_dma __user *)arg,
                       sizeof(struct adq_ioctl_dma)))
      return -EFAULT;

    /* Check that channel is valid */
    if (s.dma_struct.channel >= adq->ndma)
      return -EACCES;

    if (down_interruptible(&adq->dma.sem))
      return -EINTR;

    /* Check that device can accept DMA transfer request */
    if (((ioread32((u32 __iomem *)
                   (adq->bar0_addr +
                    XILREG_CH(s.dma_struct.channel, XILREG_WDSCSR_OFFSET)))>>8)
         & 0x3f) >= DMA_HW_QUEUE_SIZE) {
      up(&adq->dma.sem);
      return -EAGAIN;
    }

    /* Request transfer from device */
    /* Set constant address flag */
    result =
      ioread32((u32 __iomem *)
               (adq->bar0_addr +
                XILREG_CH(s.dma_struct.channel, XILREG_WDPCR_OFFSET)));
    result =
      ((result & (~XILREG_WDPCR_CONSTADDR)) |
       (s.dma_struct.options & XILREG_WDPCR_CONSTADDR));
    iowrite32(result,
              (u32 __iomem *)
              (adq->bar0_addr +
               XILREG_CH(s.dma_struct.channel, XILREG_WDPCR_OFFSET)));
    /* Set upper 32bit of DMA address */
    iowrite32(s.dma_struct.addr_hi,
              (u32 __iomem *)
              (adq->bar0_addr +
               XILREG_CH(s.dma_struct.channel, XILREG_WDUACR_OFFSET)));
    /* Set lower 32bit of DMA address */
    iowrite32(s.dma_struct.addr_lo,
              (u32 __iomem *)
              (adq->bar0_addr +
               XILREG_CH(s.dma_struct.channel, XILREG_WDLACR_OFFSET)));
    /* Set TLP count */
    iowrite32(s.dma_struct.tlp_count,
              (u32 __iomem *)
              (adq->bar0_addr +
               XILREG_CH(s.dma_struct.channel, XILREG_WDCCR_OFFSET)));
    /* Start DMA Transfer */
    iowrite32(XILREG_WDSCSR_DMA,
              (u32 __iomem *)
              (adq->bar0_addr +
               XILREG_CH(s.dma_struct.channel, XILREG_WDSCSR_OFFSET)));
    up(&adq->dma.sem);

    break;

  case ADQ_IOCTL_LOCK_DMA_READ:
    if (copy_from_user(&s, (struct adq_ioctl_dma __user *)arg,
                       sizeof(struct adq_ioctl_dma)))
      return -EFAULT;

    /* Decrease read semaphore */
    dev_dbg(&adq->pci_dev->dev, "Locking read buffer, timeout %d\n",
             ((s.dma_struct.timeout_val) * HZ) / 1000);
    if (s.dma_struct.timeout_val == 0) {
      if (down_interruptible(&adq->dma.sem_r))
        return -EINTR;
    } else {
      if (down_timeout(&adq->dma.sem_r,
                       ((s.dma_struct.timeout_val) * HZ) / 1000)) {
        dev_err(&adq->pci_dev->dev, "Timeout while locking read buffer.\n");
        return -ETIME;
      }
    }

    if (down_interruptible(&adq->dma.sem))
      return -EINTR;
    /* Update read pointer */
    adq->dma.rptr = (adq->dma.rptr + 1) % adq->dma.nalloc;
    /* Return current read pointer */
    s.dma_struct.bufnum = adq->dma.rptr;
    s.dma_struct.tlp_count = adq->dma.tlp_count[adq->dma.rptr];
    up(&adq->dma.sem);

    if (copy_to_user ((struct adq_ioctl_dma __user *)arg, &s,
                      sizeof(struct adq_ioctl_dma)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_UNLOCK_DMA_READ:
    if (copy_from_user(&s, (struct adq_ioctl_dma __user *)arg,
                       sizeof(struct adq_ioctl_dma)))
      return -EFAULT;

    dev_dbg(&adq->pci_dev->dev, "Unlocking read buffer.\n");

    atomic_dec(&(adq->dma.count_r));

    /* Increase write semaphore */
    up(&adq->dma.sem_w);

    queue_work(adq->workqueue, &adq->schedule_dma_work);

    if (copy_to_user((struct adq_ioctl_dma __user *)arg, &s,
                     sizeof(struct adq_ioctl_dma)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_RD_BUF_STATUS:
    s.response.val = atomic_read(&(adq->dma.count_r));
    if (copy_to_user((struct adq_ioctl_response __user *)arg, &s,
                     sizeof(struct adq_ioctl_response)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_WR_BUF_STATUS:
    s.response.val = atomic_read(&(adq->dma.count_w));
    if (copy_to_user((struct adq_ioctl_response __user *)arg, &s,
                     sizeof(struct adq_ioctl_response)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_WRITE_64:
    break;

  case ADQ_IOCTL_WRITE_32:
    break;

  case ADQ_IOCTL_READ_CSH:
    for (i = 0; i < ADQ_PCIE_CSH_SIZE; i++)
      pci_read_config_dword(adq->pci_dev, (i * sizeof(u32)),
                            &s.csh.data[i]);

    if (copy_to_user((struct adq_ioctl_csh __user *)arg, &s,
                     sizeof(struct adq_ioctl_csh)))
      return -EFAULT;
    break;

  case ADQ_IOCTL_WRITE_CSH:
    if (copy_from_user(&s, (struct adq_ioctl_csh __user *)arg,
                       sizeof(struct adq_ioctl_csh)))
      return -EFAULT;

    for (i = 0; i < ADQ_PCIE_CSH_SIZE; i++)
      pci_write_config_dword(adq->pci_dev, (i * sizeof(u32)),
                             s.csh.data[i]);

    /* Look up maximum packet size */
    reg = ioread32((u32 __iomem *)
                   (adq->bar0_addr +
                    XILREG_DLTSR_OFFSET));
    if ((reg & 0x7) < ((reg >> 8) & 0x7)) {
      adq->mpss = ((reg & 0x7) > 0x5) ?
        0 :
        (128 << (reg & 0x7));
    } else {
      adq->mpss = (((reg >> 8) & 0x7) > 0x5) ?
        0 :
        (128 << ((reg >> 8) & 0x7));
    }
    /* Look up number of DMA channels */
    reg =
      ioread32((u32 __iomem *) (adq->bar0_addr +
                                XILREG_DCSR_OFFSET));
    if (((reg >> 8) & 0xFF) >= 3) {
      adq->ndma = (((reg >> 16) & 0xF) + 1);
    } else {
      adq->ndma = 1;
    }

    break;

  case ADQ_IOCTL_CLEAR_SCHED:
    if (down_interruptible(&adq->dma.sem))
      return -EINTR;
    adq->dma.sched_count = 0;
    up(&adq->dma.sem);
    break;

  case ADQ_IOCTL_ABORT_DMA:
    if (copy_from_user(&s, (struct adq_ioctl_abort __user *)arg,
                       sizeof(struct adq_ioctl_abort)))
      return -EFAULT;

    dev_dbg(&adq->pci_dev->dev, "Aborting DMA.\n");
    /* Abort selected channels */
    for (ch = 0; ch < adq->ndma; ch++) {
      if (s.abort.mask & (1 << ch)) {
        iowrite32(XILREG_WDSCSR_ABORT,
                  (u32 __iomem *)
                  (adq->bar0_addr + XILREG_CH(ch, XILREG_WDSCSR_OFFSET)));
        iowrite32(0,
                  (u32 __iomem *)
                  (adq->bar0_addr + XILREG_CH(ch, XILREG_WDSCSR_OFFSET)));
      }
    }
    if (s.abort.mask & 0x1) {
      /* If host DMA channel is set in mask, reset DMA struct */
      atomic_set(&(adq->dma.abort_dma), 1);
      wait_event_interruptible(adq->wq_bottoms,
                               atomic_read(&(adq->bottoms_count)) == 0);
      atomic_set(&(adq->dma.abort_dma), 0);
      if (down_interruptible(&adq->dma.sem))
        return -EINTR;
      adq->dma.rptr = adq->dma.nalloc - 1;
      adq->dma.wptr = adq->dma.nalloc - 1;
      sema_init(&adq->dma.sem_w, adq->dma.nalloc - 1);
      sema_init(&adq->dma.sem_r, 0);
      atomic_set(&(adq->dma.count_r), 0);
      atomic_set(&(adq->dma.count_w), 0);
      atomic_set(&(adq->dma.count_w), 0);
      atomic_set(&(adq->dma.count_w), 0);
      adq->dma.sched_count = 0;
      adq->dma.sched_tlp_count = 0;
      up(&adq->dma.sem);
    }
    break;

  case ADQ_IOCTL_RESET_DMA:
    dev_dbg(&adq->pci_dev->dev, "Resetting DMA.\n");
    atomic_set(&(adq->dma.abort_dma), 1);
    iowrite32(XILREG_DCSR_RST,
              (u32 __iomem *) (adq->bar0_addr + XILREG_DCSR_OFFSET));
    /* At least for x8g2 devices, any reads while XILREG_DCSR_RST
       is set will make the card unoperational until reset. */
    iowrite32(0,
              (u32 __iomem *) (adq->bar0_addr + XILREG_DCSR_OFFSET));
    wait_event_interruptible(adq->wq_bottoms,
                             atomic_read(&(adq->bottoms_count)) == 0);
    atomic_set(&(adq->dma.abort_dma), 0);
    /* Reset DMA struct */
    if (down_interruptible(&adq->dma.sem))
      return -EINTR;
    adq->dma.rptr = adq->dma.nalloc - 1;
    adq->dma.wptr = adq->dma.nalloc - 1;
    sema_init(&adq->dma.sem_w, adq->dma.nalloc - 1);
    sema_init(&adq->dma.sem_r, 0);
    atomic_set(&(adq->dma.count_r), 0);
    atomic_set(&(adq->dma.count_w), 0);
    atomic_set(&(adq->dma.count_w), 0);
    atomic_set(&(adq->dma.count_w), 0);
    adq->dma.sched_count = 0;
    adq->dma.sched_tlp_count = 0;
    up(&adq->dma.sem);
    break;

  case ADQ_IOCTL_SET_WRITE_TARGET:
    if (arg == 0) {
      /* Target BAR0 */
      adq->bar_target_addr = adq->bar0_addr;
      adq->bar_target_len   = adq->bar0_len;
    } else if (arg == 2) {
      /* Target BAR2 */
      adq->bar_target_addr = adq->bar2_addr;
      adq->bar_target_len   = adq->bar2_len;
    } else {
      /* Invalid target */
      return -EINVAL;
    }
    break;

  default:
    return -EINVAL;
  }

  return 0;
}

/* Open for character device */
static int adq_open(struct inode *i, struct file *filp)
{
  struct adq_dev *adq;

  adq = container_of(i->i_cdev, struct adq_dev, c_dev);
  filp->private_data = adq;

  return 0;
}

/* Release (close) for character device */
static int adq_release(struct inode *i, struct file *filp)
{
  struct adq_dev *adq;

  adq = container_of(i->i_cdev, struct adq_dev, c_dev);
  return 0;
}

/* mmap for character device, maps DMA buffers to userspace */
static int adq_mmap(struct file *filp, struct vm_area_struct *vma)
{
  struct adq_dev *adq;
  unsigned int buf_num;
  unsigned long pfn_addr;
  unsigned long remap_size;

  /* Fetch ADQ device struct pointer */
  adq = filp->private_data;

  buf_num = vma->vm_pgoff;

  down(&adq->dma.sem);
  /* Check that requested buffer was allocated */
  if (!(buf_num < adq->dma.nalloc)) {
    up(&adq->dma.sem);
    return -EINVAL;
  }

  /* Check that requested size matches allocated size */
  if ((vma->vm_end - vma->vm_start) != adq->dma.buf_size) {
    up(&adq->dma.sem);
    return -EINVAL;
  }
  pfn_addr = page_to_pfn(virt_to_page(adq->dma.buf[buf_num]));
  remap_size = adq->dma.buf_size;
  up(&adq->dma.sem);

  /* TODO: Check if protection needs to be marked as uncachable
     or if any VM_ flags need to be added */
  /* Remap allocated buffer to user space */
  if (remap_pfn_range
      (vma, vma->vm_start, pfn_addr, remap_size, vma->vm_page_prot))
    return -EAGAIN;

  return 0;
}

/* Write for character device, writes to the target BAR at offset */
static ssize_t adq_write(struct file *filp, const char __user *buff,
                         size_t count, loff_t *offp)
{
  struct adq_dev *adq;
  u32 tmp[ADQ_WRITE_BUFSIZE];
  size_t left;
  size_t written;

  /* Fetch ADQ device struct pointer */
  adq = filp->private_data;

  /* Check if count is a multiple of sizeof(u32) */
  if (count % sizeof(__u32))
    return -EINVAL;

  left = count / sizeof(__u32);
  written = 0;
  if (adq->bar_target_len && (*offp < adq->bar_target_len) && (*offp >= 0)) {

    /* Write blocks of ADQ_WRITE_BUFSIZE * sizeof(__u32) */
    while (left >= ADQ_WRITE_BUFSIZE) {
      if (copy_from_user((char *)&tmp,
                         (char __user *)&buff[written],
                         ADQ_WRITE_BUFSIZE*sizeof(__u32)))
        return -EFAULT;
      iowrite32_rep((u32 __iomem *)
                    (adq->bar_target_addr + *offp),
                    &tmp,
                    ADQ_WRITE_BUFSIZE);
      written += ADQ_WRITE_BUFSIZE * sizeof(__u32);
      left -= ADQ_WRITE_BUFSIZE;
    }

    /* Write remaining part of buffer */
    if (left > 0) {
      if (copy_from_user((char *)&tmp,
                         (char __user *)&buff[written], left*sizeof(__u32)))
        return -EFAULT;
      iowrite32_rep((u32 __iomem *) (adq->bar_target_addr + *offp), &tmp, left);
      written += left * sizeof(__u32);
    }

    return written;
  }

  return -EACCES;
}

/* Read for character device, writes to the target BAR at offset */
static ssize_t adq_read(struct file *filp, char __user *buff,
                        size_t count, loff_t *offp)
{
  struct adq_dev *adq;
  u32 tmp[ADQ_READ_BUFSIZE];
  size_t left;
  size_t read;

  /* Fetch ADQ device struct pointer */
  adq = filp->private_data;

  /* Check if count is a multiple of sizeof(u32) */
  if (count % sizeof(__u32))
    return -EINVAL;

  left = count / sizeof(__u32);
  read = 0;
  if (adq->bar_target_len && (*offp < adq->bar_target_len) && (*offp >= 0)) {
    /* Read blocks of ADQ_READ_BUFSIZE * sizeof(__u32) */
    while (left >= ADQ_READ_BUFSIZE) {
      ioread32_rep ((u32 __iomem *)(adq->bar_target_addr + *offp),
                    &tmp, ADQ_READ_BUFSIZE);

      if (copy_to_user((char __user *)&buff[read],
                       (char *)tmp,
                       ADQ_READ_BUFSIZE*sizeof(__u32)))
        return -EFAULT;

      read += ADQ_READ_BUFSIZE * sizeof(__u32);
      left -= ADQ_READ_BUFSIZE;
    }

    /* Read remaining part of buffer */
    if (left > 0) {
      ioread32_rep((u32 __iomem *)(adq->bar_target_addr + *offp),
                   &tmp, left);

      if (copy_to_user((char __user *)&buff[read],
                       (char *)tmp,
                       left*sizeof(__u32)))
        return -EFAULT;

      read += left * sizeof(__u32);
    }

    return read;
  }

  return -EACCES;
}

static struct file_operations adq_fops = {
  .mmap = adq_mmap,
  .unlocked_ioctl = adq_ioctl,
  .open = adq_open,
  .write = adq_write,
  .read = adq_read,
  .release = adq_release
};

/* Device driver PCI probe */
static int probe(struct pci_dev *dev, const struct pci_device_id *id)
{
  int ret;
  int reg;
  u32 bar;
  struct adq_dev *adq;

  if (pci_enable_device(dev) != 0) {
    dev_err(&dev->dev, "Failed to enable PCI device\n");
    return -EIO;
  }

  dev_info(&dev->dev, "Found Signal Processing Devices %s, "
          "device PCIe/PXIe/uTCA bus %d slot %d function %d\n",
          (const char *)id->driver_data,
          dev->bus->number, PCI_SLOT(dev->devfn),
          PCI_FUNC(dev->devfn));

  for (bar = 0; bar < 6; bar++) {
    dev_dbg(&dev->dev, "%s BAR %d start 0x%lx end 0x%lx\n",
             (const char *)id->driver_data,
             bar,
             (long unsigned int)pci_resource_start(dev, bar),
             (long unsigned int)pci_resource_end(dev, bar));
  }

  if (pci_set_dma_mask(dev, ADQ_DMA_BIT_MASK)) {
    dev_err(&dev->dev, "PCI layer for device can't support 32-bit DMA "
            "addresses, pci_set_dma_mask failed\n");
    return -EIO;
  }

  adq = (struct adq_dev *) vmalloc(sizeof(struct adq_dev));
  if (IS_ERR(adq)) {
    ret = PTR_ERR(adq);
    dev_err(&dev->dev, "Can't allocate memory for auxillary struct\n");
    goto error_vmalloc;
  }
  adq->id = atomic_inc_return(&adq_num) - 1;
  adq->pci_dev = dev;
  adq->irq_alloc = 0;
  adq->dma.nalloc = 0;
  sema_init(&adq->dma.sem, 1);
  snprintf(adq->name, sizeof(adq->name), "%s_%d", ADQ_DEV_NAME, adq->id);

  atomic_set(&(adq->dma.abort_dma), 0);
  atomic_set(&(adq->bottoms_count), 0);
  init_waitqueue_head(&(adq->wq_bottoms));
  adq->workqueue = alloc_workqueue(adq->name,
    WQ_NON_REENTRANT|WQ_UNBOUND|WQ_HIGHPRI|WQ_MEM_RECLAIM, 1);

  INIT_WORK(&adq->schedule_dma_work, schedule_dma);

  /* Make sure bus master bit is set */
  pci_set_master(adq->pci_dev);

  /* Allocate char device */
  ret = alloc_chrdev_region(&adq->fs_dev, ADQ_FIRST_CHRDEV_MINOR,
                            ADQ_NUMBER_CHRDEV, adq->name);
  if (ret < 0) {
    pr_err("Can't allocate character device, alloc_chrdev_region\n");
    goto error_alloc_chdev_region;
  }

  /* Create device class and device so that UDEV can create a file in /dev */
  adq->cl = class_create(THIS_MODULE, adq->name);
  if (IS_ERR(adq->cl)) {
    ret = PTR_ERR(adq->cl);
    pr_err("Can't create class for character device, "
           "class_create returned NULL\n");
    goto error_class_create;
  }

  adq->cd = device_create(adq->cl, NULL, adq->fs_dev, NULL, adq->name);
  if (IS_ERR(adq->cd)) {
    ret = PTR_ERR(adq->cd);
    dev_err(&dev->dev, "Can't create device, device_create returned NULL\n");
    goto error_device_create;
  }

  cdev_init(&adq->c_dev, &adq_fops);
  if (cdev_add(&adq->c_dev, adq->fs_dev, 1)) {
    ret = -EIO;
    dev_err(&dev->dev, "Can't add character device, cdev_add returned -1\n");
    goto error_cdev_add;
  }

  if (!request_mem_region(pci_resource_start(dev, 0),
                          pci_resource_len(dev, 0), "adq")) {
    ret = -EIO;
    dev_err(&dev->dev, "Can't request mem region for BAR0\n");
    goto error_request_mem_region_bar0;
  }

  adq->bar0_len = pci_resource_len(dev, 0);
  adq->bar0_addr = (unsigned long) ioremap_nocache(pci_resource_start(dev, 0),
                                                   adq->bar0_len);
  if (adq->bar0_addr == 0) {
    ret = -ENOMEM;
    dev_err(&dev->dev, "Can't remap BAR0 mem region\n");
    goto error_ioremap_bar0;
  }

  adq->bar2_len = 0;
  if (pci_resource_len(dev, 2) > 0) {
    if (!request_mem_region(pci_resource_start(dev, 2),
                            pci_resource_len(dev, 2), "adq")) {
      ret = -EIO;
      dev_err(&dev->dev, "Can't request mem region for BAR2\n");
      goto error_request_mem_region_bar2;
    }

    adq->bar2_len = pci_resource_len(dev, 2);
#if defined(ARCH_HAS_IOREMAP_WC) && ADQ_BAR2_USE_WRITE_COMBINING
    adq->bar2_addr =
      (unsigned long) ioremap_wc(pci_resource_start(dev, 2),
                                 adq->bar2_len);
#else
    adq->bar2_addr =
      (unsigned long) ioremap_nocache(pci_resource_start(dev, 2),
                                      adq->bar2_len);
#endif
    if (adq->bar2_addr == 0) {
      ret = -ENOMEM;
      dev_err(&dev->dev, "Can't remap BAR2 mem region\n");
      goto error_ioremap_bar2;
    }
    adq->bar_target_addr = adq->bar2_addr;
    adq->bar_target_len  = adq->bar2_len;
  }

  pci_set_drvdata(dev, adq);

  /* Enable MSI if possible */
  pci_enable_msi(adq->pci_dev);

  /* Look up maximum packet size */
  reg = ioread32((u32 __iomem *) (adq->bar0_addr + XILREG_DLTSR_OFFSET));
  if ((reg & 0x7) < ((reg >> 8) & 0x7)) {
    adq->mpss = ((reg & 0x7) > 0x5) ?
      0 :
      (128 << (reg & 0x7));
  } else {
    adq->mpss =
      (((reg >> 8) & 0x7) > 0x5) ?
      0 :
      (128 << ((reg >> 8) & 0x7));
  }
  /* Look up number of DMA channels */
  reg = ioread32((u32 __iomem *) (adq->bar0_addr + XILREG_DCSR_OFFSET));
  if (((reg >> 8) & 0xFF) >= 3) {
    adq->ndma = (((reg >> 16) & 0xF) + 1);
  } else {
    adq->ndma = 1;
  }

#ifdef DEBUG
  pr_device_registers(adq);
#endif
  ret = 0;
  goto no_errors;

 error_ioremap_bar2:
  if (adq->bar2_len) {
    release_mem_region(pci_resource_start(dev, 2),
                       pci_resource_len(dev, 2));
  }
 error_request_mem_region_bar2:
  iounmap((void __iomem *)adq->bar0_addr);
 error_ioremap_bar0:
  release_mem_region(pci_resource_start(dev, 0),
                     pci_resource_len(dev, 0));
 error_request_mem_region_bar0:
  cdev_del(&adq->c_dev);
 error_cdev_add:
  device_unregister(adq->cd);
  device_destroy(adq->cl, adq->fs_dev);
 error_device_create:
  class_destroy(adq->cl);
 error_class_create:
  unregister_chrdev_region(adq->fs_dev, 1);
 error_alloc_chdev_region:
  destroy_workqueue(adq->workqueue);
  vfree(adq);
 error_vmalloc:
  pci_disable_device(dev);
 no_errors:
  return ret;
}

/* Device driver PCI remove */
static void remove(struct pci_dev *dev)
{
  struct adq_dev *adq;

  dev_info(&dev->dev, "Removing device\n");

  adq = pci_get_drvdata(dev);

  flush_workqueue(adq->workqueue);
  destroy_workqueue(adq->workqueue);

  down(&adq->dma.sem);
  unallocate_dma_buf(adq);
  up(&adq->dma.sem);

  pci_disable_msi(adq->pci_dev);
  /* Clean up from probe */
  if (adq->bar2_len) {
    iounmap((void __iomem *)adq->bar2_addr);
    release_mem_region(pci_resource_start(dev, 2),
                       pci_resource_len(dev, 2));
  }
  iounmap((void __iomem *)adq->bar0_addr);
  release_mem_region(pci_resource_start(dev, 0),
                     pci_resource_len(dev, 0));
  cdev_del(&adq->c_dev);
  device_unregister(adq->cd);
  device_destroy(adq->cl, adq->fs_dev);
  class_destroy(adq->cl);
  unregister_chrdev_region(adq->fs_dev, 1);
  vfree(adq);
  pci_set_drvdata(dev, NULL);
  pci_disable_device(dev);
}

static struct pci_driver pci_driver = {
  .name = "spd_adq_pci",
  .id_table = ids,
  .probe = probe,
  .remove = remove,
};

/* Module init */
static int __init pci_adq_init(void)
{
  int ret;
  atomic_set(&adq_num, 0);
  pr_info("SPD ADQ PCI Device driver v%d.%d loaded.\n",
          ADQ_DEVICE_DRIVER_REVISION_MAJOR,
          ADQ_DEVICE_DRIVER_REVISION_MINOR);

  /* Register PCI driver */
  ret = pci_register_driver(&pci_driver);
  if (ret)
    pr_err("Can't register PCI device.\n");

  return ret;
}

/* Module exit */
static void __exit pci_adq_exit(void)
{
  pci_unregister_driver(&pci_driver);
  pr_info("SPD ADQ PCI Device driver unloaded.\n");
}

module_init(pci_adq_init);
module_exit(pci_adq_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("ADQ PCI Devices");
MODULE_AUTHOR("Signal Processing Devices Sweden AB <support@spdevices.com>");
