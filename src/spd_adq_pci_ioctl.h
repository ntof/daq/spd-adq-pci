/*
 * Signal Processing Devices ADQ PCIe/PXIe/uTCA devices
 *
 * Signal Processing Devices Sweden AB <support@spdevices.com>
 *
 * IOCTL definitions
 *
 */

#ifndef ADQ_IOCTL_H_
#define ADQ_IOCTL_H_
#include <linux/ioctl.h>
#include <linux/types.h>

/* Device driver revision */
#define ADQ_DEVICE_DRIVER_REVISION_MAJOR 1
#define ADQ_DEVICE_DRIVER_REVISION_MINOR 16

/* Struct definitions for IOCTLs */
struct adq_ioctl_response {
  __u32 val;
};

struct adq_ioctl_alloc {
  /* Buffer size in bytes */
  __u32 size;
  /* Number of buffers */
  __u32 number;
};

struct adq_ioctl_buf {
  /* Buffer number */
  __u32 buf_num;
  /* Buffer size */
  __u32 size;
  /* Offset argument for mmap */
  __u32 mmap_offset;
  /* Bus address high part*/
  __u32 bus_addr_hi;
  /* Bus address high part*/
  __u32 bus_addr_lo;
};

struct adq_ioctl_reg {
  /* Register offset */
  __u32 offset;
  /* Bit mask for register write */
  __u32 mask;
  /* Value for register write */
  __u32 val;
};

struct adq_ioctl_dma {
  /* Timeout in ms */
  __u32 timeout_val;
  /* Bus address, high part*/
  __u32 addr_hi;
  /* Bus address, low part*/
  __u32 addr_lo;
  /* Number of TLPs to transfer*/
  __u32 tlp_count;
  /* Buffer number */
  __u32 bufnum;
  /* Transfer count */
  __u32 count;
  /* Channel */
  __u32 channel;
  /* Options */
  __u32 options;
};

struct adq_ioctl_device_info {
  /* Device vendor ID */
  __u32 vid;
  /* Device product ID */
  __u32 pid;
  /* Device bus number */
  __u32 bus;
  /* Device slot number */
  __u32 slot;
  /* Maximum packet send size */
  __u32 mpss;
};

struct adq_ioctl_abort {
  /* Channel mask */
  __u32 mask;
};

#define ADQ_PCIE_CSH_SIZE 0x43
struct adq_ioctl_csh{
  /* Configuration space header data */
  __u32 data[ADQ_PCIE_CSH_SIZE];
};

/* IOCTL IDs */
#define ADQ_IOCTL_DRIVER_REV        _IOR ('a', 0, struct adq_ioctl_response *)
#define ADQ_IOCTL_ALLOC_DMA_BUF     _IOWR('a', 1, struct adq_ioctl_alloc *)
#define ADQ_IOCTL_FETCH_DMA_BUF     _IOWR('a', 2, struct adq_ioctl_buf *)
#define ADQ_IOCTL_DEVICE_WR_REG     _IOW ('a', 3, struct adq_ioctl_reg *)
#define ADQ_IOCTL_DEVICE_RD_REG     _IOWR('a', 4, struct adq_ioctl_reg *)
#define ADQ_IOCTL_REQUEST_DMA_WRITE _IOW ('a', 5, struct adq_ioctl_dma *)
#define ADQ_IOCTL_REQUEST_DMA_P2P   _IOW ('a', 6, struct adq_ioctl_dma *)
#define ADQ_IOCTL_LOCK_DMA_READ     _IOWR('a', 7, struct adq_ioctl_dma *)
#define ADQ_IOCTL_UNLOCK_DMA_READ   _IOWR('a', 8, struct adq_ioctl_dma *)
#define ADQ_IOCTL_DEVICE_INFO       _IOR ('a', 9, struct adq_ioctl_device_info *)
#define ADQ_IOCTL_DEVICE_PHYSADDR   _IOWR('a', 10, struct adq_ioctl_response *)
#define ADQ_IOCTL_RD_BUF_STATUS     _IOR ('a', 11, struct adq_ioctl_response *)
#define ADQ_IOCTL_WR_BUF_STATUS     _IOR ('a', 12, struct adq_ioctl_response *)
#define ADQ_IOCTL_WRITE_64          _IO  ('a', 13)
#define ADQ_IOCTL_WRITE_32          _IO  ('a', 14)
#define ADQ_IOCTL_READ_CSH          _IOR ('a', 15, struct adq_ioctl_csh *)
#define ADQ_IOCTL_WRITE_CSH         _IOW ('a', 16, struct adq_ioctl_csh *)
#define ADQ_IOCTL_CLEAR_SCHED       _IO  ('a', 17)
#define ADQ_IOCTL_RESET_DMA         _IO  ('a', 18)
#define ADQ_IOCTL_ABORT_DMA         _IO  ('a', 19)
#define ADQ_IOCTL_SET_WRITE_TARGET  _IO  ('a', 20)

/* BAR 0 register offsets */
#define XILREG_DCSR_OFFSET   0x00
#define XILREG_DLWSR_OFFSET  0x04
#define XILREG_DLTSR_OFFSET  0x08
#define XILREG_DLTCR_OFFSET  0x0c

#define XILREG_WDPCR_OFFSET  0x40
#define XILREG_WDICSR_OFFSET 0x44
#define XILREG_WDLACR_OFFSET 0x48
#define XILREG_WDUACR_OFFSET 0x4c
#define XILREG_WDCCR_OFFSET  0x50
#define XILREG_WDSCSR_OFFSET 0x54

#define XILREG_CHANNEL_STRIDE 0x100
#define XILREG_CH(ch, reg) ((reg) + (XILREG_CHANNEL_STRIDE)*(ch))

/* Interesting bits in BAR 0 registers */
#define XILREG_WDICSR_INT      0x02
#define XILREG_WDICSR_DISINT   0x01
#define XILREG_DCSR_RST        0x01
#define XILREG_WDSCSR_DMA      0x01
#define XILREG_WDSCSR_ABORT    0x02
#define XILREG_WDPCR_CONSTADDR 0x80

/* Device controller command interface registers */
#define ADQREG_CMD_MSG_OFFSET  0x80
#define ADQREG_CMD_ARG1_OFFSET 0x84
#define ADQREG_CMD_ARG2_OFFSET 0x88
#define ADQREG_CMD_ARG3_OFFSET 0x8C
#define ADQREG_DRY_BUSY_OFFSET 0x90
#define ADQREG_CPU_OUT_OFFSET  0x94

/* Interesting bits in command interface registers */
#define ADQREG_DRY_BUSY_INT    0x1

#endif
